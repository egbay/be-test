const express = require("express");
const cors = require("cors");
const cookieSession = require("cookie-session");

const dbConfig = require("./app/config/db.config");
const logger = require("./app/utils/logger");

require('dotenv').config();

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// Parse requests of content-type - application/json
app.use(express.json());

// Parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use(
  cookieSession({
    name: "session",
    secret: process.env.SECRET,
    httpOnly: true
  })
);

// Database configurations
const db = require("./app/models");

db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    logger.info("Connected to database successfully.");
  })
  .catch(err => {
    logger.error("Connection error", err);
    process.exit();
  });

// test main route
app.get("/", (req, res) => {
  logger.info("Welcome to product catalog management application.");
  res.json({ message: "Welcome to product catalog management application." });
});


// Routes
require("./app/routes/category.routes")(app);
require("./app/routes/product.routes")(app);
require("./app/routes/auth.routes")(app);


// Set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  logger.info(`Server is running on port ${PORT}.`);
});

module.exports = app;
