const { createLogger, format, transports } = require("winston");
const { combine, timestamp, printf, colorize, align } = format;


const logLevels = {
  fatal: 0,
  error: 1,
  warn: 2,
  info: 3,
  debug: 4,
  trace: 5,
};

module.exports = createLogger({
  levels: logLevels,
  level: process.env.LOG_LEVEL || 'info',
  defaultMeta: {
    service: 'product-service',
  },
  format: combine(
    colorize({ all: true }),
    format.splat(),
    format.json(),
    timestamp({
      format: 'YYYY-MM-DD hh:mm:ss.SSS A',
    }),
    align(),
    printf((info) => `[${info.timestamp}] ${info.level}: ${info.message}`)
  ),
  transports: [new transports.Console()],
});