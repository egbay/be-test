
function isString(item){
  let abc = typeof item;

  if (abc=="string"){
    return true;
    }
  else {
    return false;
  }
}

function isNumber(item){
  let abc = typeof item;

  if (abc=="number"){
    return true;
    }
  else {
    return false;
  }
}

function getUserId(req){

  if (req.headers["authorization"]){
    try {
      const bearerToken = JSON.stringify(req.headers["authorization"]);
      let token = bearerToken.split(" ")[1];
      let base64Url = token.split('.')[1];
      let base64 = base64Url.replace('-', '+').replace('_', '/');
      let decodedData = JSON.parse(Buffer.from(base64, 'base64').toString('binary'));
      let userId = decodedData.id;
      return userId;
    }
    catch (err) {
      return null;
    }

  }
  else {
    return null;
  }

}

module.exports = { isString, isNumber, getUserId };
