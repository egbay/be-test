let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let schema = new Schema(
    {
      name: String,
      description: String
    },
      { timestamps: true }
    );

schema.virtual('id').get(function () {
    return this._id;
});

schema.set('toJSON', {
    virtuals: true,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.__v;
    }
});

module.exports = mongoose.model("category", schema);

