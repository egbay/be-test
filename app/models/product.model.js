let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// { type: Schema.Types.ObjectId, ref: "Category", required: true}

let schema = new Schema(
    {
      title: String,
      description: String,
      price: Number,
      categoryId: String,
      userId: String
    },
      { timestamps: true }
    );

schema.virtual('id').get(function () {
    return this._id;
});

schema.set('toJSON', {
    virtuals: true,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.__v;
    }
});

module.exports = mongoose.model("product", schema);
