require('dotenv').config();

const hostName = process.env.DATABASE_HOSTNAME;
const port = process.env.DATABASE_PORT;
const databaseName = process.env.DATABASE_NAME;

module.exports = {
  HOST: hostName,
  PORT: port,
  DB: databaseName
};