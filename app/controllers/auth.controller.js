var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

const config = require("../config/auth.config");
const db = require("../models");
const logger = require("../utils/logger");
const { isString } = require("../utils/helper");

const User = db.user;

exports.signup = (req, res) => {
  // Validate request
  if (!req.body.email || !req.body.password) {
    let response = {
      message: "Payload is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
    return;
  }
  // Validate payload types
  else if (!isString(req.body.email) || !isString(req.body.password)) {
    let response = {
      message: "Payload types are not correct."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
    return;
  }

  const user = new User({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
  });

  user.save((err, user) => {
    if (err) {
        res.status(500).send({ message: err });
        return;
    }
    else {
        let response = { message: "Registration has been completed successfully. Please use /login to get access token." };
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.info('%o', {...logObject});
        res.send(response);
    }
  });
};

exports.signin = (req, res) => {

  // Validate request
  if (!req.body.email || !req.body.password) {
    let response = {
      message: "Payload is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
    return;
  }
  // Validate payload types
  else if (!isString(req.body.email) || !isString(req.body.password)) {
    let response = {
      message: "Payload types are not correct."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
    return;
  }

  User.findOne({
    email: req.body.email,
  })
    .exec((err, user) => {
      if (err) {
        let response = { message: err };
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.error('%o', {...logObject});
        res.status(500).send(response);
        return;
      }

      if (!user) {
        let response = { message: "User does not exist." };
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.error('%o', {...logObject});
        return res.status(404).send(response);
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        let response = { message: "Password is invalid." };
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.error('%o', {...logObject});
        return res.status(401).send(response);
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400, // 24 hours
      });

      req.session.token = token;

      let response = {
        id: user._id,
        email: user.email,
        accessToken: token,
      };
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.info('%o', {...logObject});
      res.status(200).send(response);
    });
};

exports.signout = async (req, res) => {
  try {
    req.session = null;
    let response = { message: "You have signed out successfully" };
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.info('%o', {...logObject});
    return res.status(200).send(response);
  } catch (err) {
    let response = { message: err};
    let logObject = {"method": req.method, "url": req.url, "message": err}
    logger.error('%o', {...logObject});
    this.next(err);
  }
};
