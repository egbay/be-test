const db = require("../models");
const logger = require("../utils/logger");
const { isString } = require("../utils/helper");

const Category = db.category;


// Create a new category
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name || !req.body.description) {
    let response = {
      message: "Payload is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
    return;
  }
  else if (!isString(req.body.name) || !isString(req.body.description)) {
    let response = {
      message: "Payload types are not correct."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
    return;
  }

  // Create a Category
  const category = new Category({
    name: req.body.name,
    description: req.body.description
  });

  // Save Category in the database
  category
    .save(category)
    .then(data => {
      let logObject = {"method": req.method, "url": req.url, "message": data}
      logger.info('%o', {...logObject});
      res.status(201).send(data);
    })
    .catch(err => {
      let response = {
        message: err.message || "Unknown error occurred while creating category."
      }
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});
      res.status(500).send(response);
    });
};

// Retrieve all categories
exports.getAll = (req, res) => {

  Category.find().sort({updatedAt: -1})
    .then(data => {
      let logObject = {"method": req.method, "url": req.url, "message": data}
      logger.info('%o', {...logObject});
      res.send(data);
    })
    .catch(err => {
      let response = {
        message:
          err.message || "Unknown error occurred while retrieving categories."
      }
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});
      res.status(500).send(response);
    });
};

// Delete a category by id
exports.delete = (req, res) => {
  // Validate if id is missing
  const id = req.params.id;
  if (!id) {
    let response = {
      message: "Id is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});

    return res.status(400).send(response);
  }

  Category.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        let response = { message: "Category does not exist with id." };
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.error('%o', {...logObject});
        res.status(404).send(response);
      } else {
        let response = {"message": "Category has been deleted successfully" }
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.info('%o', {...logObject});
        res.send(response);
      }
    })
    .catch(err => {
      let response = { message: "Could not delete category with id=" + id };
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});
      res
        .status(500)
        .send(response);
    });
};
