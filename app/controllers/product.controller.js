const {get} = require("mongoose");

const db = require("../models");
const logger = require("../utils/logger");
const { isString, isNumber, getUserId } = require("../utils/helper");

const Product = db.product;

// Create a new product
exports.create = (req, res) => {

  // Validate request
  if (!req.body.title || !req.body.description || !req.body.price || !req.body.categoryId) {
    let response = {
      message: "Payload is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
    return;
  }
  // Validate payload types
  else if (!isString(req.body.title) || !isString(req.body.description) || !isNumber(req.body.price) || !isString(req.body.categoryId)) {
    let response = {
      message: "Payload types are not correct."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
    return;
  }


  // Create a product object
  const product = new Product({
    title: req.body.title,
    description: req.body.description,
    price: Number(req.body.price),
    categoryId: req.body.categoryId,
    userId: getUserId(req)
  });

  // Save product
  product
    .save(product)
    .then(data => {
      let logObject = {"method": req.method, "url": req.url, "message": data}
      logger.info('%o', {...logObject});
      res.status(201).send(data);
    })
    .catch(err => {
      let response = {
        message:
          err.message || "Unknown error occurred while creating product."
      }
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});
      res.status(500).send(response);
    });
};

// Retrieve all products
exports.getAll = (req, res) => {

  // title and categoryId are possible query parameters
  const title = req.query.title;
  const categoryId = req.query.categoryId;

  const userId = getUserId(req);

  // Validate if userId is valid?
  if (!userId) {
    let response = {
      message: "Token is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    return res.status(401).send(response);
  }

  const userCondition = userId ? { userId: userId } : {};
  const titleCondition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};
  const categoryCondition = categoryId ? { categoryId: categoryId } : {};

  let condition = Object.assign(userCondition, titleCondition, categoryCondition);

  console.log(condition);
  Product.find(condition).sort({updatedAt: -1})
    .populate('category')
    .then(data => {
      let logObject = {"method": req.method, "url": req.url, "message": data}
      //logger.info('%o', {...logObject});
      res.send(data);
    })
    .catch(err => {
      let response = {
        message:
          err.message || "Unknown error occurred while retrieving products."
      }
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});
      res.status(500).send(response);
    });
};

// Find a single product by id
exports.getOne = (req, res) => {

  // Validate if id is missing
  const id = req.params.id;
  if (!id) {
    let response = {
      message: "Id is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});

    return res.status(400).send(response);
  }

  const userId = getUserId(req);

  // Validate if userId is valid?
  if (!userId) {
    let response = {
      message: "Token is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    return res.status(401).send(response);
  }

  Product.findById(id)
    .then(data => {
      if (!data){
        let response = { message: "Product does not exist with id." };
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.error('%o', {...logObject});
        res.status(404).send(response);
      }
      else{
        if (data['userId']===userId) {
          let logObject = {"method": req.method, "url": req.url, "message": data}
          logger.info('%o', {...logObject});
          res.send(data);
        }
        else {
          let response = { message: "Request is unauthorized." };
          let logObject = {"method": req.method, "url": req.url, "message": response}
          logger.error('%o', {...logObject});
          res.status(401).send(response);
        }
      }
    })
    .catch(err => {
      let response = { message: "An error occurred retrieving product with id=" + id };
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});
      res
        .status(500)
        .send(response);
    });
};

// Update a product by id
exports.update = (req, res) => {

  // Validate if id is missing
  const id = req.params.id;
  if (!id) {
    let response = {
      message: "Id is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    return res.status(400).send(response);
  }

  // Validate if payload is empty
  if (!req.body || Object.keys(req.body).length === 0) {
    let response = {
      message: "Payload is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});
    res.status(400).send(response);
  }

  // Validate if request has different parameters
  let hasExtraParameter = false;
  const payloadKeys = Object.keys(req.body);
  const possibleParameters = ["title", "description", "price", "categoryId"];
  for (let item in payloadKeys) {
    hasExtraParameter = possibleParameters.includes(item);
    if (hasExtraParameter){
      let response = {
        message: "Payload is not valid."
      }
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});
      res.status(400).send(response);
      return res.status(400).send({
        message: "Payload is not valid."
      });
    }
  }

  Product.findByIdAndUpdate(id, req.body, { new: true, useFindAndModify: false })
    .then(data => {
      //console.log(data);
      if (!data) {
        let response = { message: "Product does not exist with this id." };
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.error('%o', {...logObject});
        res.status(404).send(response);
      } else {

        let logObject = {"method": req.method, "url": req.url, "message": data}
        logger.info('%o', {...logObject});
        res.send(data);
      }
    })
    .catch(err => {
      let response = { message: "An error occurred updating product with id=" + id };
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});

      res.status(500).send(response);
    });
};

// Delete a product by id
exports.delete = (req, res) => {

  // Validate if id is missing
  const id = req.params.id;
  if (!id) {
    let response = {
      message: "Id is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});

    return res.status(400).send(response);
  }

  Product.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        let response = { message: "Product does not exist with id." };
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.error('%o', {...logObject});
        res.status(404).send(response);

      }
      else {
        let response = {"message": "Product has been deleted successfully" }
        let logObject = {"method": req.method, "url": req.url, "message": response}
        logger.info('%o', {...logObject});
        res.send(response);
      }
    })
    .catch(err => {
      let response = { message: "Could not delete product with id=" + id };
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});
      res
        .status(500)
        .send(response);
    });
};
