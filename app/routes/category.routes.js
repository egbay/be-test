const {authJwt} = require("../middlewares");
const category = require("../controllers/category.controller.js");

require('dotenv').config();
const version = process.env.API_VERSION;
const baseUrl = `/api/${version}`

module.exports = app => {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, Content-Type, Accept"
    );
    next();
  });

  // Get categories
  app.get(`${baseUrl}/categories`, [authJwt.verifyToken], category.getAll);

  // Create a new category
  app.post(`${baseUrl}/category`, [authJwt.verifyToken], category.create)

  // Delete a category with id
  app.delete(`${baseUrl}/category/:id`, [authJwt.verifyToken], category.delete);

};
