const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/auth.controller");

require('dotenv').config();
const version = process.env.API_VERSION;
const baseUrl = `/api/${version}`

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
    `${baseUrl}/signup`,
    [
      verifySignUp.checkDuplicateEmail
    ],
    controller.signup
  );

  app.post(`${baseUrl}/login`, controller.signin);

  app.post(`${baseUrl}/signout`, controller.signout);
};
