const {authJwt} = require("../middlewares");
const product = require("../controllers/product.controller.js");

require('dotenv').config();
const version = process.env.API_VERSION;
const baseUrl = `/api/${version}`

module.exports = app => {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, Content-Type, Accept"
    );
    next();
  });

  // Get products related to user
  app.get(`${baseUrl}/products`, [authJwt.verifyToken], product.getAll);

  // Get a single product by id
  app.get(`${baseUrl}/product/:id`, [authJwt.verifyToken], product.getOne);

  // Create a new product
  app.post(`${baseUrl}/product`, [authJwt.verifyToken], product.create)

  // Update a product with id
  app.put(`${baseUrl}/product/:id`, [authJwt.verifyToken], product.update);

  // Delete a product with id
  app.delete(`${baseUrl}/product/:id`, [authJwt.verifyToken], product.delete);

};