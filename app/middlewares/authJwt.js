const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const logger = require("../utils/logger");
const db = require("../models");
const User = db.user;

verifyToken = (req, res, next) => {
  // Read from session
  let token = req.session.token;

  // If session is clean, read from request's header.
  if (typeof token=="undefined" || typeof token==null) {
    if (req.headers["authorization"]){
      let bearerToken = JSON.stringify(req.headers["authorization"]);
      token = bearerToken.split(" ")[1];
      token = token.slice(0, -1);
    }
    else {
      token = null;
    }
  }

  if (!token) {
    let response = {
      message: "Token is missing."
    }
    let logObject = {"method": req.method, "url": req.url, "message": response}
    logger.error('%o', {...logObject});

    return res.status(401).send(response);
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      let response = {
        message: "Request is unauthorized."
      }
      let logObject = {"method": req.method, "url": req.url, "message": response}
      logger.error('%o', {...logObject});

      return res.status(401).send(response);
    }
    req.userId = decoded.id;
    next();
  });
};

const authJwt = {
  verifyToken
};
module.exports = authJwt;
