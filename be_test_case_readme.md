<h1>Backend Development (Node.js) Technical Test</h1>

<strong>The challenge</strong>

Your challenge is to develop an API, using Node.JS, for a product catalog management application. Thus, you must analyze and convert the user stories below into routes of an application.
 
<strong>User stories:</strong>

- As a user I would like to register a product so that I can have access to the data of this product in the future (Title, description, price, category)
- I as a user would like to be able to associate and edit a product category;
- As a user I would like to be able to access the list of all products;
- As a user I would like to be able to filter products by name or category;
- I as a user would like to be able to update the product data;
- I as a user would like to be able to delete a product from my catalog;
 
<strong>Instructions</strong>
- <strong>To start the test, create a .git repository, and send us the link to the test performed (invite paulo.ribeiro@smartex.ai & shrujal.shah@smartex.ai) .</strong>
- The choice of libraries, databases, architecture, etc. is at your discretion.
- Create a README file explaining what it takes to run your application.
- If you want you can leave us feedback regarding the test

 
<strong>Our analysis</strong>
- Knowledge of Javascript, NodeJs will be assessed for this position;
- We'll look at how you structure the:
  application layers;
  outgoing calls,
  environment variables,
   cache,
  unit tests,
  logs;
  error handling;
  documentation.
- Code organization, module separation, readability and comments.
- Commit history.
