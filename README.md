<div id="top"></div>


<!-- PROJECT LOGO -->
<br />
<div align="center">

  <h3 align="center">Product Catalog Management API v1</h3>


</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#prerequisites">User Story</a></li>
        <li><a href="#prerequisites">Database Schema</a></li>
        <li><a href="#prerequisites">Endpoints</a></li>
        <li><a href="#prerequisites">Project Directory</a></li>
        <li><a href="#prerequisites">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#backlog">Backlog</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Product catalog management application built with Node.JS as for a backend technical test.


### User Story
* As a user I would like to register a product so that I can have access to the data of this product in the future (Title, description, price, category)
* I as a user would like to be able to associate and edit a product category;
* As a user I would like to be able to access the list of all products;
* As a user I would like to be able to filter products by name or category;
* I as a user would like to be able to update the product data;
* I as a user would like to be able to delete a product from my catalog;


### Database Schema
* user
  * id, email, password
* product
  * id, title, description, price, categoryId, userId, updatedAt, createdAt
* category
  * id, name, description, updatedAt, createdAt

### Endpoints
* User Related
  * signup
  * login
  * signout
* Product Related
  * post_product
  * get_products
  * get_products_by_name
  * get_products_by_category
  * put_product_category
  * put_product
  * delete_product
* Category Related
  * post_category
  * get_categories
  * delete_category
  
### Project Directory

  ```sh
  \---be-test-egb
    |   .env
    |   .gitignore
    |   README.md
    |   package-lock.json
    |   package.json
    |   server.js
    |   
    \---app
        +---config
        |       auth.config.js
        |       db.config.js
        |
        +---controller
        |       auth.controller.js
        |       category.controller.js
        |       product.controller.js
        |       
        +---middlewares
        |       authJwt.js
        |       index.js
        |       verifySignUp.js
        |       
        +---models
        |       category.model.js
        |       index.js
        |       product.model.js
        |       user.model.js
        |       
        +---routes
        |       auth.routes.js
        |       index.js
        |       category.routes.js
        |       product.routes.js
        |       
    +---logs
    |       error.log
    |       info.log
    +---test
    |       test.js
    |       
  ```



<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* ExpressJS
* NodeJS
* MongoDB
* ChaiJS
* MochaJS


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started


You can find information about how you could set up project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

You need to install npm and node, if you don't have it please install it with the command below.
* npm
  ```sh
  npm install npm@8.5.1 -g
  ```
* node (this project built on v17.6.0)
  * [https://nodesource.com/blog/installing-nodejs-tutorial-windows/](https://nodesource.com/blog/installing-nodejs-tutorial-windows/)
    
### Installation

_To install this project properly, please follow the installation steps below._

1. Clone this repository
   ```sh
   git clone https://github.com/egbay/be-test-egb.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Install MongoDB on your local
* For MacOS
  * [https://www.mongodb.com/docs/v4.2/tutorial/install-mongodb-on-os-x/](https://www.mongodb.com/docs/v4.2/tutorial/install-mongodb-on-os-x/)

4. Install MongoDB Compass
* For MacOS, Windows, Linux
  * [https://www.mongodb.com/docs/compass/current/install/](https://www.mongodb.com/docs/compass/current/install/)

5. Create testdb on MongoDB
* For MacOS
  * [https://www.mongodb.com/docs/compass/current/databases/](https://www.mongodb.com/docs/compass/current/databases/)

6. Insert environment variables inside a file called `.env`
   ```sh
    # .env file
    DATABASE_HOSTNAME="127.0.0.1"
    DATABASE_PORT = "27017"
    DATABASE_NAME = "testdb"
    
    PORT = "8080"
    SECRET = "COOKIE_SECRET"
    NODE_ENV="dev"
    API_VERSION = "v1"
   ```
7. Start application
    ```sh
   npm start
   ```
8. Start test operation
    ```sh
   npm test
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

1. Insert category (for products)
2. Signup with new user
3. Login with your account
4. Post new product
5. Get all products
6. Get filtered products
7. Update product's price, category, title, description
8. Delete a product

_For more examples, please refer to the [Documentation](https://documenter.getpostman.com/view/5993671/UyxgH7t4)_

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- BACKLOG -->
## Backlog
- [ ] Checking if categoryId is valid in post_product and put_product_category endpoints.
- [ ] Showing category name on get_products, post_product, put_product endpoints.
- [ ] Adding currency parameter for price.
- [ ] Adding new collection for currencies.
- [ ] Implementing currency parameter for post_product, put_product, get_products endpoints.
- [ ] Environment name could be implemented on baseUrl (i.e.: localhost:8080/api/v1/dev)
- [ ] Testing should be taken place on another database.
- [ ] Status code, exception cases, trackId should be added in log objects.
- [ ] Pagination could be implemented for get_products endpoint.
- [ ] Logging could be on file or other service like NewRelic, Cloudwatch instead of console.
- [ ] New test cases could be implemented;
  - [ ] Success and fail cases for login, signup endpoints
  - [ ] Checking response object with expected values, types, length in depth
  - [ ] Checking with invalid categoryId on post_product, put_product endpoints
  - [ ] Unauthorized requests
  - [ ] Token control with each endpoint
- [ ] Code has some repetition. Refactoring is needed.


<p align="right">(<a href="#top">back to top</a>)</p>

