//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
const Product = require("../app/models/product.model");


//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

let mockToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyN2ExZmZjZjRhNjRlMjhkYWZhYmM0NyIsImlhdCI6MTY1MjIwOTc3MCwiZXhwIjoxNjUyMjk2MTcwfQ.LQJ_XtFl9h-4dwvEh-ZnslX5PqZq5uvh-gsTagm3f3c";
mockToken = "Bearer " + mockToken;
let mockProductSuccess = {
    "title": "test product #3",
    "description": "test description",
    "price": 100,
    "categoryId": "627a395894e75c4365627e55"
}

let mockProductFail = {
    "description": "test description",
    "price": 100,
    "categoryId": "627a395894e75c4365627e55"
}

let mockProductUpdate = {
    "title": "test product #3 v2",
    "description": "test description",
    "price": 150,
    "categoryId": "627a395894e75c4365627e55"
}

const baseUrl = "/api/v1";


chai.use(chaiHttp);
// Testing for Product resource
 describe('Product', () => {
    beforeEach((done) => { //Before each test we empty the database
        Product.deleteMany({}, (err) => {
           done();
        });
    });

  /*
  * Test the /GET products without token
  */
  describe('/GET products (Fail)', () => {
      it('Should not retrieve all products without token', (done) => {
        chai.request(server)
            .get(baseUrl + '/products')
            .end((err, res) => {
                  res.should.have.status(401);
              done();
            });
      });
  });


  /*
  * Test the /GET products with token
  */
  describe('/GET products (Success)', () => {
      it('Should retrieve all products with token', (done) => {
        chai.request(server)
            .get(baseUrl + '/products')
            .set('Authorization', mockToken)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('array');
                  res.body.length.should.be.eql(0);
              done();
            });
      });
  });

  /*
  * Test the /POST product for a fail case
  */
  describe('/POST product (Fail)', () => {
      it('Should not post a product with missing parameter', (done) => {
        chai.request(server)
            .post(baseUrl + '/product')
            .set('Authorization', mockToken)
            .send(mockProductFail)
            .end((err, res) => {
                  res.should.have.status(400);
              done();
            });
      });
  });

  /*
  * Test the /POST product for success case
  */
  describe('/POST product (Success)', () => {
      it('Should post a product', (done) => {
        chai.request(server)
            .post(baseUrl + '/product')
            .set('Authorization', mockToken)
            .send(mockProductSuccess)
            .end((err, res) => {
                  res.should.have.status(201);
              done();
            });
      });
  });

  /*
  * Test the /PUT product
  */
  describe('/PUT product (Success)', () => {
      it('Should update a product', (done) => {
        let product = new Product(mockProductSuccess);
        product.save((err, product) => {
            chai.request(server)
            .put(baseUrl + '/product/' + product._id)
            .set('Authorization', mockToken)
            .send(mockProductUpdate)
            .end((err, res) => {
                  res.should.have.status(200);
              done();
            });
        });
      });
  });

  /*
  * Test the /DELETE product
  */
  describe('/DELETE product (Success)', () => {
      it('Should delete a product', (done) => {
        let product = new Product(mockProductSuccess);
        product.save((err, product) => {
            chai.request(server)
            .delete(baseUrl + '/product/' + product._id)
            .set('Authorization', mockToken)
            .end((err, res) => {
                  res.should.have.status(200);
              done();
            });
        });
      });
  });

});